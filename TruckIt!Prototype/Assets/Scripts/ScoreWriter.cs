﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreWriter : MonoBehaviour {

    public Text ScoreText;
    public bool IsPlayScene;
	// Use this for initialization
	void Start () {
        ScoreText.text = "SCORE: " + Storage.Instance.GetScore();
	}
	
	// Update is called once per frame
	void Update () {
        if (IsPlayScene)
        {
            ScoreText.text = "SCORE: " + ScoreManager.Instance.GetScore();
        }
    }
}
