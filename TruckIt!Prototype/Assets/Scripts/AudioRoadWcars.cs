﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioRoadWcars : MonoBehaviour {

    public AudioClip honk1;
    public AudioClip honk2;
    public AudioClip honk3;
    public AudioClip honk4;

    public AudioSource MusicSource;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter(Collider col)
    {
        int clip = Random.Range(0, 5);

        if (clip == 1)
        {
            MusicSource.clip = honk1;
            MusicSource.Play();
        }

        if (clip == 2)
        {
            MusicSource.clip = honk2;
            MusicSource.Play();
        }

        if (clip == 3)
        {
            MusicSource.clip = honk3;
            MusicSource.Play();
        }

        if (clip == 4)
        {
            MusicSource.clip = honk4;
            MusicSource.Play();
        }
    }
}