﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour {

    public AudioClip Fiddle;
    public AudioClip Bum_Bum;
    public AudioClip Soundtrack;

    public AudioSource MusicSource;

    // Use this for initialization
    void Start () {
        int clip = Random.Range(0, 4);
	
        if (clip <= 1)
        {
            MusicSource.clip = Bum_Bum;
            MusicSource.Play();
        }

        if (clip < 3)
        {
	    MusicSource.volume = .5f;
            MusicSource.clip = Fiddle;
            MusicSource.Play();
        }

        else
        {
            MusicSource.clip = Soundtrack;
            MusicSource.Play();
        }

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
