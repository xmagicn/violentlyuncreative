﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceOverWriter : MonoBehaviour {

    public Text DistanceText;

    // Use this for initialization
    void Start()
    {
        DistanceText.text = "You Drove " + Storage.Instance.GetDistance() + " Feet!";
    }

}
