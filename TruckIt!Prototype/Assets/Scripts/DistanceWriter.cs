﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceWriter : MonoBehaviour {
    public Text DistanceText;

    // Use this for initialization
    void Start () {
        DistanceText.text = "Distance: ";
    }
	
	// Update is called once per frame
	void Update () {
        DistanceText.text = "Distance: " + DistanceTracker.Instance.GetDistanceTraveled();
    }
}
