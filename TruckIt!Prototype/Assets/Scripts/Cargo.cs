﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cargo : MonoBehaviour {

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision col){
        if (col.gameObject.tag == "Terrain")
        {
            // Debug.Log("Cargo Fall");
            CargoManager.Instance.CargoLost();
            Destroy(gameObject);
        }
    }
}
