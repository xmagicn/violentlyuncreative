﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainTile : MonoBehaviour {
    
    const int MAXPOSITIONS = 10;

    public Transform[] CoinPositions = new Transform[MAXPOSITIONS];
    public int numCoinPositions;

    public Transform[] PickUpPositions = new Transform[MAXPOSITIONS];
    public int numPickUpPositions;
    
    public TerrainManager rTerrainManager;

    public Transform TileStart;
    public Transform TileEnd;
    public float TileRot;
    
    public string tileName;

    void Start()
    {
    }

    private void Awake()
    {
        rTerrainManager = GameObject.FindGameObjectWithTag("TerrainManager").GetComponent("TerrainManager") as TerrainManager;
    }

    public void SpawnPU(Transform puPrefab, Transform pos)
    {
        if (numPickUpPositions > 0)
        {
            int i = Random.Range(0, numPickUpPositions);
            privSpawnItem(puPrefab, PickUpPositions[i].position, pos.position);
        }
    }

    public void SpawnCoins(Transform coinPrefab, Transform pos)
    {
        for (int i = 0; i < numCoinPositions; i++)
        {
            privSpawnItem(coinPrefab, CoinPositions[i].position, pos.position);
        }
    }

    private void privSpawnItem(Transform prefab, Vector3 itemPos, Vector3 tilePos)
    {
        Transform transf = Instantiate(prefab, itemPos + tilePos, Quaternion.identity);
        transf.transform.RotateAround(tilePos, new Vector3(0, 1, 0), rTerrainManager.GetCurrentRotAngle());
    }
    
    public void TileEntered()
    {
        rTerrainManager.RecieveTerrainTrigger();
        rTerrainManager.MoveOffRoadZone(this.transform);
    }
}