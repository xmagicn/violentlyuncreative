﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainIDKBonus : MonoBehaviour
{
    bool Entered = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!Entered)
        {
            Entered = true;
            TerrainTile tile = GetComponentInParent<TerrainTile>();
            tile.TileEntered();
        }
    }
}
