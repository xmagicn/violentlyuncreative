﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum TerrainOutputCode
{
    TERRAIN_SUCCESS,

    TERRAIN_EMPTY_ARRAY,

    TERRAIN_FAILURE
}

public class TerrainManager : MonoBehaviour
{
    Queue<Transform> currentTerrain; // Currently active terrain

    public int numStraightTiles;
    List<Transform> straightTiles;
    public Transform[] rawStraightTiles = new Transform[MaxNumTiles];

    public int numLeftTiles;
    List<Transform> leftTiles;
    public Transform[] rawLeftTiles = new Transform[MaxNumTiles];

    public int numRightTiles;
    List<Transform> rightTiles;
    public Transform[] rawRightTiles = new Transform[MaxNumTiles];

    public int StartingRoadLen;
    List<Transform> startingRoad;
    public Transform[] rawStartingRoad = new Transform[MaxNumTiles];
    
    public static int MaxNumTiles = 20;

    public int maxActiveTerrain;
    public int minActiveTerrain;

    public int TerrainBuffer;
    int BufferCount = 0;
    
    public Vector3 currentRoadEnd;
    public float currentRotAngle = 0.0f;

    public DistanceTracker tracker;

    public GameObject DeadZone;
    float DeadZoneOffset = 20.0f;

    public GameObject OffRoadTracker;
    float OffRoadOffset = 10.0f;

    public float PUThresholdValue;
    public Transform CoinPrefab;
    public Transform PUPrefab;

    private List<Transform> ChooseNextDirection()
    {
        float randFloat = Random.value;

        if (randFloat < 0.34f) return straightTiles;
        else if (randFloat < 0.67f) return leftTiles;
        else return rightTiles;
    }

    int ChooseNextTile(int len)
    {
        return Random.Range(0, len);
    }

    // Wrapper for currentTerrain push
    private void PushTile(Transform t)
    {
        // Push tile t to currentTerrain
        currentTerrain.Enqueue(t);
    }

    // Wrapper for currentTerrain pop
    private Transform PopTile()
    {
        // Pop tile
        Transform poppedTile = null;

        // Spawn tiles if neccessary

        return poppedTile;
    }

    private void InitializeTile(TerrainTile tile, Transform tran)
    {        
        // Place and rotate tile
        tran.position -= tile.TileStart.position;
        tran.transform.RotateAround(currentRoadEnd, new Vector3(0, 1, 0), currentRotAngle);

        tile.rTerrainManager = this;

        // Spawn ur shit
        tile.SpawnCoins(CoinPrefab, tran);
        if (Random.value > PUThresholdValue) tile.SpawnPU(PUPrefab, tran);
    }

    private void UpdateManager(Transform tran)
    {
        TerrainTile tile = tran.gameObject.GetComponent("TerrainTile") as TerrainTile;

        UpdateRoadEnd(tile.TileEnd.position);
        UpdateRoadFwd(tile.TileRot);

        PushTile(tran);
    }
    
    private void SpawnTile()
    {
        // Randomly select next tile
        List<Transform> selectedList;
        do
        {
            selectedList = ChooseNextDirection();
        } while (selectedList.Count < 1);

        int i = ChooseNextTile(selectedList.Count);

        // Load Tile
        TerrainTile tile = selectedList[i].gameObject.GetComponent("TerrainTile") as TerrainTile;
        Transform tran = PrivLoadTile(selectedList, currentRoadEnd, i);

        // Initialize
        InitializeTile(tile, tran);

        // Update the road end
        UpdateManager(tran);
    }

    private void SpawnTile(TerrainTile tile, Transform tran)
    {
        InitializeTile(tile, tran);
        UpdateManager(tran);
    }

    private void DespawnTile()
    {
        // Remove tile from currentTerrain
        Transform doneTile = currentTerrain.Dequeue();

        // Clean up and delete tile
        Destroy(doneTile.gameObject);
    }
    
    private TerrainOutputCode LoadRawTiles(Transform[] rawTiles, List<Transform> output, int numTiles)
    {
        if (rawTiles.Length < 1) return TerrainOutputCode.TERRAIN_EMPTY_ARRAY;
        
        for(int i = 0; i < numTiles; i++)
        {
            output.Add(rawTiles[i]);
        }

        return TerrainOutputCode.TERRAIN_SUCCESS;
    }
    
    private Transform PrivLoadTile(List<Transform> tiles, Vector3 start, int i)
    {
        return Instantiate(tiles[i], start, tiles[i].transform.rotation);
    }

    private void SpawnInitialRoad()
    {
        for (int i = 0; i < StartingRoadLen; i++)
        {
            // Instantiate tile
            TerrainTile tile = startingRoad[i].gameObject.GetComponent("TerrainTile") as TerrainTile;
            Transform tran = PrivLoadTile(startingRoad, currentRoadEnd, i);

            InitializeTile(tile, tran);

            UpdateManager(tran);
        }

        while (currentTerrain.Count < maxActiveTerrain)
        {
            SpawnTile();
        }
    }

    private void InitializeStructures()
    {
        startingRoad = new List<Transform>();
        straightTiles = new List<Transform>();
        rightTiles = new List<Transform>();
        leftTiles = new List<Transform>();
        
        currentTerrain = new Queue<Transform>();

        currentRoadEnd = new Vector3(0, 0, 0);
    }

    // Use this for initialization
    void Start()
    {
        InitializeStructures();

        if (LoadRawTiles(rawStartingRoad, startingRoad, StartingRoadLen) != TerrainOutputCode.TERRAIN_SUCCESS) Debug.Log("Error loading starting road");
        if (LoadRawTiles(rawStraightTiles, straightTiles, numStraightTiles) != TerrainOutputCode.TERRAIN_SUCCESS) Debug.Log("Error loading straight tiles");
        if (LoadRawTiles(rawRightTiles, rightTiles, numRightTiles) != TerrainOutputCode.TERRAIN_SUCCESS) Debug.Log("Error loading right tiles");
        if (LoadRawTiles(rawLeftTiles, leftTiles, numLeftTiles) != TerrainOutputCode.TERRAIN_SUCCESS) Debug.Log("Error loading left tiles");

        SpawnInitialRoad();
    }

    private void UpdateRoadEnd(Vector3 vec)
    {
        currentRoadEnd = vec;
    }

    void UpdateRoadFwd(float delta)
    {
        currentRotAngle += delta;
    }

    public Vector3 GetCurrentRoadEnd()
    {
        return currentRoadEnd;
    }

    public float GetCurrentRotAngle()
    {
        return currentRotAngle;
    }

    public void RecieveTerrainTrigger()
    {
        if (BufferCount >= TerrainBuffer)
        {
            DespawnTile();
            
            SpawnTile();
        }
        else
        {
            BufferCount++;
        }
    }
    
    public void MoveOffRoadZone(Transform CurrTile)
    {
        DeadZone.transform.position = new Vector3(0, CurrTile.position.y - DeadZoneOffset, 0);

        OffRoadTracker.transform.position = new Vector3(CurrTile.position.x, CurrTile.position.y - OffRoadOffset, CurrTile.position.z);
    }
}

