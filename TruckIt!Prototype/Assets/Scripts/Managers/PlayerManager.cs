﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour {

    public GameObject Player;
    public GameObject PlayerHead;
    public Transform CoinSpawner;
    public CoinCargo CoinSpawn;
    MovementControl PlayerMovement;

    public static int NumCoinSpawnPointsMax = 20;
    public int NumCoinSpawnPoints;
    public Transform[] CoinSpawnList = new Transform[NumCoinSpawnPointsMax];
    private int Count = 0;

    private static PlayerManager instance;
    public static PlayerManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new PlayerManager();
            }
            return instance;
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            DestroyImmediate(this);
        }
    }

    // Use this for initialization
    void Start () {
        PlayerMovement = Player.GetComponent("MovementControl") as MovementControl;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject GetPlayer()
    {
        return Player;
    }

    public GameObject GetPlayerHead()
    {
        return PlayerHead;
    }
    
    public void SlowDownPlayerBy(int Slow)
    {
        PlayerMovement.SlowDown(Slow);
    }

    public void EndGame()
    {
        SceneManager.LoadScene("PlayAgainMenu", LoadSceneMode.Single);
    }

    public void SpawnCoin()
    {
       Instantiate(CoinSpawn, CoinSpawnList[Count].position, CoinSpawner.transform.rotation);
       // test.transform.parent = gameObject.transform;

        if (Count < NumCoinSpawnPoints-1)
        {
            Count++;
        }
        else
        {
            Count = 0;
        }
    }
}
