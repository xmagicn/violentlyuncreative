﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Storage : MonoBehaviour {
    /// <summary>
    /// Put any values we may need between scenes here.
    /// </summary>

    int Score;
    float Distance;

    private static Storage instance;
    public static Storage Instance
    {
        get
        {
            if (instance == null)   //disable this to get rid of warning but will throw errors if game doesnt start from main menu.  As storage wont exist for text ref.
            {
                instance = new Storage();
            }
            return instance;
        }
    }
    
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            DestroyImmediate(this);
        }
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int GetScore()
    {
        return Score;
    }

    public void SetScore(int newScore)
    {
        Score = newScore;
    }

    public float GetDistance()
    {
        return Distance;
    }

    public void SetDistance(float newDistance)
    {
        Distance = newDistance;
    }
}
