﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargoManager : MonoBehaviour {
    int CargoMax;
    int CargoCurr;
    private GameObject[] getCount;

    private static CargoManager instance;
    public static CargoManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new CargoManager();
            }
            return instance;
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            DestroyImmediate(this);
        }
    }


    // Use this for initialization
    void Start () {
        getCount = GameObject.FindGameObjectsWithTag("Cargo");
        CargoMax = getCount.Length;
        CargoCurr = CargoMax;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void WakeUp()
    {
        //empty to create instance in GameManager.
        Start();
    }

    public void CargoLost()
    {
      //  Debug.Log("Cargo Lost");
        CargoCurr--;
        Debug.Log(CargoCurr);
        if (CargoCurr <= 0)
        {
      //      Debug.Log("Game Over!");
         //   Application.LoadLevel("PlayAgainMenu");
        }
    }
}
