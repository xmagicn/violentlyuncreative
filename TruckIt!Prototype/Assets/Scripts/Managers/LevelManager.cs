﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Difficulty
{
    STARTER_DIFF,
    EASY_DIFF,
    MEDIUM_DIFF,
    HARD_DIFF,
    FINAL_DIFF
}

public class DifficultyState
{
    public DifficultyState nextState;
    Difficulty difficulty;
    public int threshhold;

    public DifficultyState(DifficultyState nState, Difficulty diff, int thresh)
    {
        nextState = nState;
        difficulty = diff;
        threshhold = thresh;
    }
}

public class LevelManager : MonoBehaviour
{
    public const int EASY_THRESHHOLD = 5;
    public const int MEDIUM_THRESHHOLD = 15;
    public const int HARD_THRESHHOLD = 30;
    public const int FINAL_THRESHHOLD = int.MaxValue;

    int startingRoadLen;

    int counter = 0;
    int startingCounter;

    /*====== States ======*/
    static DifficultyState final = new DifficultyState(null, Difficulty.FINAL_DIFF, FINAL_THRESHHOLD);
    static DifficultyState hard = new DifficultyState(final, Difficulty.HARD_DIFF, HARD_THRESHHOLD);
    static DifficultyState medium = new DifficultyState(hard, Difficulty.MEDIUM_DIFF, MEDIUM_THRESHHOLD);
    static DifficultyState easy = new DifficultyState(medium, Difficulty.EASY_DIFF, EASY_THRESHHOLD);

    DifficultyState currDifficulty = easy;

    public void Start()
    {
        
    }

    // Only counted after the starting road is spawned
    public void LevelManagerCallback()
    {
        counter++;
        if (counter > currDifficulty.threshhold)
        {
            currDifficulty = currDifficulty.nextState;
        }
    }

}
