﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    private int Score;

    private static ScoreManager instance;
    public static ScoreManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new ScoreManager();
            }
            return instance;
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            DestroyImmediate(this);
        }
    }

    // Use this for initialization
    void Start () {
        Score = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int GetScore()
    {
        return this.Score;
    }

    public void AddScore(int Points)
    {
        this.Score += Points;
   //     Debug.Log("Score: " + this.Score);
    }

    private void OnDestroy()
    {
        Storage.Instance.SetScore(Score);
    }
}
