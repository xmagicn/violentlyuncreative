﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedometerWriter : MonoBehaviour
{

    public Text SpeedText;

    private Rigidbody PlayerBody;
    float MPH;
    int CounterMax = 300;
    int Counter;
    // Use this for initialization
    void Start()
    {
        PlayerBody = PlayerManager.Instance.GetPlayerHead().GetComponent<Rigidbody>();
        SpeedText.text = "Speed: 0 MPH";
        Counter = CounterMax;
    }

    // Update is called once per frame
    void Update()
    {
        MPH = Mathf.RoundToInt(PlayerBody.velocity.magnitude * 2.237f * 1.5f);
        if (MPH <= 10)
        {
            Counter--;
            if (Counter <= 0)
            {
                PlayerManager.Instance.EndGame();
            }
        }
        else
        {
            Counter = CounterMax;
        }
        SpeedText.text = "Speed: " + MPH + " MPH";
    }
}