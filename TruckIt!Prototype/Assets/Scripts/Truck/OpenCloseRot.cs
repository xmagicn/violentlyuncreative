﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenCloseRot : MonoBehaviour {
 
    GameObject wall;
    bool RightUp;
    bool LeftUp;
    int RotSpeed;

    // Use this for initialization
    void Start () {
        RotSpeed = 2;
    }

    void FixedUpdate () {
        UpdateWalls();
    }

    void UpdateWalls()
    {        
        wall = GameObject.FindWithTag("RightWall");
        if (Input.GetKey(KeyCode.F) && !LeftUp)
        {
            RightUp = true;
            if (wall.transform.localRotation.eulerAngles.z < 88)
            {
                wall.transform.Rotate(0, 0, RotSpeed);
            }

        }
        else
        {
            LowerWall(wall);
        }

        wall = GameObject.FindWithTag("LeftWall");
        if (Input.GetKey(KeyCode.G) && !RightUp)
        {
            LeftUp = true;
          //  Debug.Log(wall.transform.rotation.eulerAngles.z); //negative angles are ass when the wall is set to a non negative z value
            if(wall.transform.localRotation.eulerAngles.z > 280)
            {
                wall.transform.Rotate(0, 0, -RotSpeed);
            }

        }
        else
        {
            LowerWall(wall);
        }
    }

    void LowerWall(GameObject gO)
    { 

        if (wall.tag == "LeftWall")
        {
            if(wall.transform.localRotation.eulerAngles.z < 350)
            {
                wall.transform.Rotate(0, 0, RotSpeed);
            }

            LeftUp = false;
        }
        else
        {

            if (wall.transform.localRotation.eulerAngles.z > 6)
            {
                wall.transform.Rotate(0, 0, -RotSpeed);
            }

            RightUp = false;

        }
    }
}
