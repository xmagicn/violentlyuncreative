﻿using UnityEngine;
using System.Collections;

public class MovementControl : MonoBehaviour {
    public WheelCollider[] wheelColliders = new WheelCollider[4];
    public Transform[] tyreMeshes = new Transform[4];
    
    private Rigidbody m_rigidbody;
    public Transform centerOfMass;

    public GameObject RightSignal;
    public GameObject LeftSignal;
    GameObject truck, trailer;

    //Speed values
    public float acceleration;
    public float ChangePerSecond = 1;
    public float maxTorque = 40.0f;
    public float MinSpeed = 500;
    public float MaxSpeed = 10000;

    float PixotAngle = 30;
    private float Straighten;

    public AudioClip Skidd;

    public AudioSource MusicSource;

    float timeLeft = 3.0f;

    void start()
	{
		m_rigidbody = GetComponent<Rigidbody>();
		m_rigidbody.centerOfMass = centerOfMass.localPosition;


        MusicSource.clip = Skidd;
        MusicSource.Play();

    }
	
	void Update()
	{
		UpdateMeshesPositions ();
	}

    public void SlowDown(int Slow)
    {
        acceleration -= Slow;
    }
	
	void FixedUpdate()
	{// 0 is front left and 1 is front right
		float steer = Input.GetAxis ("Horizontal");

        ManageTurnSignal(steer);

        PlayAudio(steer);

		float fixedAngel = steer * PixotAngle;
		wheelColliders [0].steerAngle = fixedAngel;
		wheelColliders [1].steerAngle = fixedAngel;

        acceleration = Mathf.Clamp(acceleration + ChangePerSecond * Time.deltaTime, MinSpeed, MaxSpeed);
       // Debug.Log("GOTTA GO FAST: " + acceleration);

        for (int i = 0; i < wheelColliders.Length; i++) 
		{
			wheelColliders[i].motorTorque = acceleration * maxTorque;
        }
    }

	void ManageTurnSignal(float steer)
    {
        //Turn signals, can change to manager to turn on or off on key stroke to get rid of needless checks
        if (steer < 0)
        {
            RightSignal.SetActive(false);
            LeftSignal.SetActive(true);
        }
        else if (steer > 0)
        {
            RightSignal.SetActive(true);
            LeftSignal.SetActive(false);
        }
        else
        {
            RightSignal.SetActive(false);
            LeftSignal.SetActive(false);
            //GetTruckTrailer();
            //ReAlignTruck();
        }
    }
	void UpdateMeshesPositions()
	{
		for(int i = 0; i < tyreMeshes.Length; i++)
		{
			Quaternion quat;
			Vector3 pos;
			wheelColliders[i].GetWorldPose(out pos, out quat);
	//		tyreMeshes[i].position = pos;
	//		tyreMeshes[i].rotation = quat;
		}
	}
    
    //void GetTruckTrailer()
    //{
    //    truck = GameObject.FindGameObjectWithTag("PlayerHead");
    //    trailer = GameObject.FindGameObjectWithTag("PlayerTrailer");
    //}

    //void ReAlignTruck()
    //{
    //    Rigidbody truckRB = truck.GetComponent<Rigidbody>();
    //    Rigidbody trailerRB = trailer.GetComponent<Rigidbody>();

    //    Vector3 truckVel = truckRB.velocity;
    //    Vector3 trailerVel = trailerRB.velocity;

    //    Vector3 truckTar = truck.transform.position + truckVel;
    //    Vector3 trailerTar = trailer.transform.position + trailerVel;



    //    Vector3 truckFwd = truck.transform.forward;
    //    Vector3 trailerFwd = trailer.transform.forward;
    //    Vector3 targetVector = (truckFwd + trailerFwd) / 2.0f;
    //    Vector3 targetPos = trailer.transform.position + targetVector;

    //    float maxVelocity = 800000f;

    //    Vector3 Htarget = targetPos - truckTar;
    //    Vector3 Vdesired = Htarget.normalized * maxVelocity;
    //    Vector3 steering = Vdesired - truckVel;

    //  //  Debug.Log(steering.ToString());
    //    truckRB.AddForce(steering);

    //    Debug.DrawRay(truck.transform.position, truckVel, Color.blue, 0, false);
    //    Debug.DrawRay(trailer.transform.position, trailerVel, Color.red, 0, false);
    //    Debug.DrawRay(trailer.transform.position, targetVector, Color.green, 0, false);
    //    Debug.DrawRay(truckTar, steering, Color.black, 0, false);
    //}

    void PlayAudio(float steer)
    {
        if (MusicSource)
          //  Debug.Log("timer" + timeLeft);
        if (steer < 0)
        {

            timeLeft -= Time.deltaTime;
            if (timeLeft < 0)
            {
                MusicSource.mute = false;
            }
        }

        else if (steer > 0)
        {

            timeLeft -= Time.deltaTime;
            if (timeLeft < 0)
            {
                MusicSource.mute = false;
            }
        }

        else
        {
            MusicSource.mute = true;
            timeLeft = 3.0f;
        }
    }   
}
