﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceTracker : MonoBehaviour {

    public Transform StartPoint;
    private float StartHeight;
    public Transform PlayerCenter;
    private int Points = 5;
    private int HeightIncrament = 5;
    private bool PlayerOffRoad = false;
    float check;

    private static DistanceTracker instance;
    public static DistanceTracker Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new DistanceTracker();
            }
            return instance;
        }
    }
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            StartHeight = StartPoint.position.y;
        }
        else
        {
            DestroyImmediate(this);
        }
    }


    // Update is called once per frame
    void Update()
    {

        if (!PlayerOffRoad)
        {
            if ((int)(StartHeight - PlayerCenter.position.y) % HeightIncrament == 0)
            {
                ScoreManager.Instance.AddScore(Points);
                HeightIncrament += 5;
            }
        }

    }

    public void SetPlayerOffRoad(bool OnOrOff)
    {
        PlayerOffRoad = OnOrOff;
    }

    public float GetDistanceTraveled()
    {
        Storage.Instance.SetDistance(StartHeight - PlayerCenter.position.y);
        return StartHeight - PlayerCenter.position.y;   
    }
}
