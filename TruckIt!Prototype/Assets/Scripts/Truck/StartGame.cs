﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartGame : MonoBehaviour {

    public Button button;

    public void changemenuscene(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }
    
    public void EndGame()
    {
        Application.Quit();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
           SceneManager.LoadScene("FoxTerrainScene", LoadSceneMode.Single);
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }
    } 
}
