﻿using UnityEngine;
using System.Collections;

public class DraggableCart : MonoBehaviour
{
    public WheelCollider[] wheelColliders = new WheelCollider[4];
    public Transform[] tyreMeshes = new Transform[4];
    public float maxTorque = 40.0f;
    private Rigidbody m_rigidbody;
    public Transform centerOfMass;
    float acceleration = 0;

    void start()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_rigidbody.centerOfMass = centerOfMass.localPosition;
        // acceleration = 500.0f; //moved to FixedUpdate becuase doesn't seem to be recognized when placed here.
    }

    void Update()
    {
        UpdateMeshesPositions();
    }

    void FixedUpdate()
    {// 0 is front left and 1 is front right
        /*
        float steer = Input.GetAxis("Horizontal");
        float fixedAngel = steer * 45f;
        wheelColliders[0].steerAngle = fixedAngel;
        wheelColliders[1].steerAngle = fixedAngel;
        //*/

        //acceleration = Input.GetAxis ("Vertical");
        //acceleration = 500.0f;         //If this value is not set within fixed update then the truck won't accelerate.  Don't ask me why.
        for (int i = 0; i < wheelColliders.Length; i++)
        {
            wheelColliders[i].motorTorque = acceleration * maxTorque;
        }
    }


    void UpdateMeshesPositions()
    {
        for (int i = 0; i < tyreMeshes.Length; i++)
        {
            Quaternion quat;
            Vector3 pos;
            wheelColliders[i].GetWorldPose(out pos, out quat);
            tyreMeshes[i].position = pos;
      //       tyreMeshes[i].rotation = quat;
        }
    }
}
