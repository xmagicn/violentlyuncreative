﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallUpPU : MonoBehaviour {

    private OpenCloseRot RotScript;
    private int Timer;
    private int RotSpeed = 2;
    private bool RightDown;
    private bool LeftDown;
    private GameObject wall;
    private bool Lower;
    private bool Raise;
    // Use this for initialization
    void Start () {
        RotScript = GameObject.FindGameObjectWithTag("Player").GetComponent<OpenCloseRot>();
        Raise = false;
        Lower = false;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Raise)
        {
           RaiseWalls();
        }

        if (Lower)
        {
            LowerWalls();
        }
    }

    void RaiseWalls()
    {
        wall = GameObject.FindWithTag("RightWall");
   
        if (wall.transform.localRotation.eulerAngles.z < 88)
        {
            wall.transform.Rotate(0, 0, RotSpeed);
        }

        wall = GameObject.FindWithTag("LeftWall");
        if (wall.transform.localRotation.eulerAngles.z > 280)
        {
            wall.transform.Rotate(0, 0, -RotSpeed);
        }
    }

    void LowerWalls()
    {
        wall = GameObject.FindWithTag("RightWall");

        if (wall.transform.localRotation.eulerAngles.z > 6)
        {
            wall.transform.Rotate(0, 0, -RotSpeed);
        }
        else
        {
            RightDown = true;
        }

        wall = GameObject.FindWithTag("LeftWall");
        if (wall.transform.localRotation.eulerAngles.z < 350)
        {
            wall.transform.Rotate(0, 0, RotSpeed);
        }
        else
        {
            LeftDown = true;
        }


        if (LeftDown && RightDown)
        {
            RotScript.enabled = true;
            Lower = false;
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Raise = true;

        RotScript.enabled = false;

        StartCoroutine(RevertWallControl());

        gameObject.GetComponent<Collider>().enabled = false;

        gameObject.GetComponent<Renderer>().enabled = false;
    }

    IEnumerator RevertWallControl()
    {
        
        yield return new WaitForSeconds(10);

        Lower = true;
        Raise = false;

    }

}
