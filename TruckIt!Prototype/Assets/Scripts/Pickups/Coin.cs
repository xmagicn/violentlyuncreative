﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 

public class Coin : MonoBehaviour {

    private int Points = 2;

    public AudioClip coin1;
    public AudioClip coin2;
    public AudioClip coin3;

    public AudioSource MusicSource;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {

        int clip = Random.Range(0, 4);

        if (clip == 1)
        {
            MusicSource.clip = coin1;
            MusicSource.Play();
        }

        if (clip == 2)
        {
            MusicSource.clip = coin2;
            MusicSource.Play();
        }

        if (clip == 3)
        {
            MusicSource.clip = coin3;
            MusicSource.Play();
        }

        ScoreManager.Instance.AddScore(Points);
        Destroy(gameObject);
        PlayerManager.Instance.SpawnCoin();

        
    }

}
