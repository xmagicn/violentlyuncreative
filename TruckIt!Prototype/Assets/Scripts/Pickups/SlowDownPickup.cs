﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowDownPickup : MonoBehaviour {


    public AudioClip Pickup;

    public AudioSource MusicSource;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider col)
    {
   
        Destroy(gameObject);

        MusicSource.clip = Pickup;
        MusicSource.Play();
    }
    
}
