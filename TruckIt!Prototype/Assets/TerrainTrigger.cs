﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainTriggerWall : MonoBehaviour {

    public Transform prefab;
    bool Entered = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!Entered)
        {
            Entered = true;
            TerrainTile tile = prefab.gameObject.GetComponent("TerrainTile") as TerrainTile;
            tile.TileEntered();
            Debug.Log("Tile Trigger");
        }
    }
}
